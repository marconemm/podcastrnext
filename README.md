# Podcastr

This is a SPA application based on the knowledge got on the *New Level Week* (**NLW**) event made by [Rocketseat](https://rocketseat.com.br/).

### Considerations:
*   Was applied the *"Server Side Rendering"*(**SSR**) and *"Static Site Generation"* (**SSG**) concept.
    * Due the objective in this project was to practice only the ***"Next.js capabilities"***.