import "../styles/reset.css";
import "../styles/globalStyles.css";
import appStyles from "../styles/app.module.css";
import React from "react";
import { Header } from "../components/Header";
import { Player } from "../components/Player";

function MyApp({ Component, pageProps }) {
	const classes = `displayFlex ${appStyles.wrapper}`; // to accept multiples classes.

	return (
		<div className={classes}>
			<main>
				<Header />
				<Component {...pageProps} />
			</main>
            <Player/>
		</div>
	);
}

export default MyApp;
