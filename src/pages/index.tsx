import { useEffect } from "react";

const SERVER_URL = "http://localhost:3100/episodes";

export default function Home(props: { episodes: any; }) {
	//Single Page Application (SPA): get the information from Back-end, via an HTTP request, parse it to JSON and consume this data onto "client side" application.
	// useEffect(() => {
	// 	fetch(SERVER_URL)
	// 		.then(response => response.json())
	// 		.then(responseJSON => console.log(responseJSON));
	// }, []);

	return (
		<>
			<h1>Index</h1>
      <p>{JSON.stringify(props.episodes)}</p>
		</>
	);
} // Home()

/**
=> Server Side Rendering (SSR): get the information from Back-end, via an HTTP request, parse it to JSON and make it already processed to the "client side" application build its DOM.

=> SSR is compulsory on every application that demands a SEO over the processed Back-end data.
*/

// must be "getInitialProps" or "getServerSideProps" on name.
// export async function getServerSideProps() {
// 	const result = {
// 		// must be "props" on the key:
// 		props: {
// 			episodes: undefined,
// 		},
// 	};

// 	const response = await fetch(SERVER_URL);
// 	result.props.episodes = await response.json();

// 	return result;
// }; // getServerSideProps()

/**
=> Static Site Generation (SSG): Makes the Back-end create a static copy from the application DOM on every "pre determinate interval" to delivery it to every client that try to access this website into this re-rendering interval. */

// must be "getStaticProps" on name.
export async function getStaticProps() {
	const result = {
		// must be "props" on the key:
		props: {
			episodes: undefined,
		},
		revalidate: 60 * 60 * 8, // It's pre determinate interval" in seconds. In this case on each 8h (28.800s).
	};

	const response = await fetch(SERVER_URL);
	result.props.episodes = await response.json();

	return result;
} // getStaticProps()
