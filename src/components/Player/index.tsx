import playerStyles from "./styles.module.css";

// PLAYER GLOBAL CONSTANTS:
const DISPLAY_FLEX_CLASS = "displayFlex";
const BTN_CLASS = "btn";

export function Player() {
	const sectionClasses = `${DISPLAY_FLEX_CLASS} displayFlex--col ${playerStyles.wrapper}`; // to accept multiples classes.
	const emptyPlayerClasses = `${DISPLAY_FLEX_CLASS} ${playerStyles.emptyPlayer}`;
	const progressBarClasses = `${DISPLAY_FLEX_CLASS} ${playerStyles.progress}`;
	const btnClasses = `${DISPLAY_FLEX_CLASS} ${BTN_CLASS} ${playerStyles.btn}`;
	const playBtnClasses = `${DISPLAY_FLEX_CLASS} ${BTN_CLASS} ${playerStyles.btn} ${playerStyles.btnPlay}`;

	return (
		<section className={sectionClasses}>
			<header className={DISPLAY_FLEX_CLASS}>
				<img src="/playing.svg" alt="Tocando agora" />
				<strong>Tocando agora</strong>
			</header>

			<div className={emptyPlayerClasses}>
				<strong>Por favor, selecione um podcast para ouvir.</strong>
			</div>
			<footer className={playerStyles.empty}>
				<div className={progressBarClasses}>
					<span>00:00</span>
					<div className={playerStyles.slider}>
						<div className={playerStyles.emptySlider} />
					</div>
					<span>00:00</span>
				</div>
				<nav className={DISPLAY_FLEX_CLASS}>
					<button className={btnClasses}>
						<img src="/shuffle.svg" alt="Embaralhar" />
					</button>
					<button className={btnClasses}>
						<img src="/play-previous.svg" alt="Tocar anterior" />
					</button>
					<button className={playBtnClasses}>
						<img src="/play.svg" alt="Tocar" />
					</button>
					<button className={btnClasses}>
						<img src="/play-next.svg" alt="Tocar próxima" />
					</button>
					<button className={btnClasses}>
						<img src="/repeat.svg" alt="Repetir" />
					</button>
				</nav>
			</footer>
		</section>
	);
}
