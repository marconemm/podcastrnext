import format from "date-fns/format";
import ptBR from "date-fns/locale/pt-BR";
import headerStyles from "./styles.module.css";

export function Header() {

	const dateConfig = {
		locale: ptBR,
	};

	const currentDate = format(new Date(), "EEEEEE, d MMMM", dateConfig);

	const classes = `displayFlex ${headerStyles.wrapper}`; // to accept multiples classes.
	return (
		<header className={classes}>
			<img src="/logo.svg" alt="Podcastr" />
			<p>O melhor para Você ouvir, sempre!</p>
			<span>{currentDate}</span>
		</header>
	);
}
